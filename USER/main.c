#include "delay.h"
#include "usart.h"
#include "timer.h"
#include "exti.h" 
#include "lcd.h"
#include "led.h"
#include "sys.h"
#include "key.h"


volatile u8 m=0; //分钟
volatile u8 s=0; //秒位
volatile u8 ms=0;//毫秒位，1个ms代表10毫秒，如果显示毫秒，屏幕刷不过来

int main(void)
{ 
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2	 
	LED_Init();		  	//LED初始化
	delay_init();	    //延时函数初始化
	uart_init(115200);//LCD与主控通过串口通信进行初始化，不可省略
 	LCD_Init();       //LCD初始化
	EXTIX_Init();		  //外部中断初始化
	POINT_COLOR=RED;  //设置颜色
  LCD_ShowString(65,25,12*9,24,24,"stopwatch");//显示标题
	LCD_ShowChar(60+3*12,80,'m',24,0);//12像素宽，24像素长
	LCD_ShowChar(60+6*12,80,'s',24,0);
	TIM3_Int_Init(100,7199);//时钟周期72M/(7199+1)=10k,10Khz的计数频率，计数到计数到100，为10ms  
  while(1) 
	{		 
			LCD_ShowxNum(60+0*12,80,m,2,24,0);//显示
			LCD_ShowxNum(60+4*12,80,s,2,24,0);
			LCD_ShowxNum(60+7*12,80,ms,2,24,0);
	} 
	return 0;
}
